/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2017 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
  * Flow in a 2D cavity.
  **/

#include "palabos2D.h"
#include "palabos2D.hh"   // include full template code

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace plb;
using namespace plb::descriptors;

typedef double T;
#define NS_DESCRIPTOR D2Q9Descriptor
#define AD_DESCRIPTOR AdvectionDiffusionD2Q5Descriptor

// Global variables for conveniently passing data in functions.

static plint nx, ny;
static T dx, dt;
static T uLB;

void cavitySetup(MultiBlockLattice2D<T,NS_DESCRIPTOR>& ns_lattice,
        OnLatticeBoundaryCondition2D<T,NS_DESCRIPTOR>& boundaryCondition)
{
    Box2D top(0, nx-1, ny-1, ny-1);
    Box2D bottom(0, nx-1, 0,    0);
    Box2D left(0, 0, 1, ny-2);
    Box2D right(nx-1, nx-1, 1, ny-2);

    boundaryCondition.setVelocityConditionOnBlockBoundaries(ns_lattice, top);
    boundaryCondition.setVelocityConditionOnBlockBoundaries(ns_lattice, bottom);
    boundaryCondition.setVelocityConditionOnBlockBoundaries(ns_lattice, left);
    boundaryCondition.setVelocityConditionOnBlockBoundaries(ns_lattice, right);

    T u = uLB;
    setBoundaryVelocity(ns_lattice, top, Array<T,2>(u,(T)0.) );
    setBoundaryVelocity(ns_lattice, bottom, Array<T,2>((T)0.,(T)0.) );
    setBoundaryVelocity(ns_lattice, left, Array<T,2>((T)0.,(T)0.) );
    setBoundaryVelocity(ns_lattice, right, Array<T,2>((T)0.,(T)0.) );

    initializeAtEquilibrium(ns_lattice, ns_lattice.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );
    initializeAtEquilibrium(ns_lattice, top, (T)1., Array<T,2>(u,(T)0.) );

    ns_lattice.initialize();
}

void writePpm(MultiBlockLattice2D<T,NS_DESCRIPTOR>& ns_lattice, MultiBlockLattice2D<T,AD_DESCRIPTOR>& ad_lattice, plint iter)
{
    static plint iFile = 0;

    // iFile = iter;
    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledPpm(createFileName("uNorm", iFile, 6), *computeVelocityNorm(ns_lattice));
    imageWriter.writeScaledPpm(createFileName("logUnorm", iFile, 6),
            *computeLog(*add((T)1.e-8,*computeVelocityNorm(ns_lattice))));
    imageWriter.writeScaledPpm(createFileName("scalar", iFile, 6), *computeDensity(ad_lattice));

    iFile++;
}

template<class BlockLatticeT>
void writeVTK(BlockLatticeT& ns_lattice, plint iter)
{
    static plint iFile = 0;

    // iFile = iter;
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iFile, 6), dx);
    vtkOut.writeData<float>(*computeVelocityNorm(ns_lattice), "velocityNorm", dx/dt);
    vtkOut.writeData<2,float>(*computeVelocity(ns_lattice), "velocity", dx/dt);

    iFile++;
}

template<typename T,
         template<typename U1> class DESCRIPTOR1,
         template<typename U2> class DESCRIPTOR2 >
class VelocityCoupling2D : public BoxProcessingFunctional2D_LL<T, DESCRIPTOR1, T, DESCRIPTOR2>
{
public:
    VelocityCoupling2D() { }
    virtual void process(Box2D domain, BlockLattice2D<T,DESCRIPTOR1>& fluid, BlockLattice2D<T,DESCRIPTOR2>& scalar);
    virtual VelocityCoupling2D<T,DESCRIPTOR1,DESCRIPTOR2>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
};

template<typename T,
         template<typename U1> class DESCRIPTOR1,
         template<typename U2> class DESCRIPTOR2 >
void VelocityCoupling2D<T,DESCRIPTOR1,DESCRIPTOR2>::process(Box2D domain, BlockLattice2D<T,DESCRIPTOR1>& fluid,
                                                                          BlockLattice2D<T,DESCRIPTOR2>& scalar)
{
    // TASK: WRITE THE CONTENT OF THE DATA PROCESSOR
}

template<typename T,
         template<typename U1> class DESCRIPTOR1,
         template<typename U2> class DESCRIPTOR2 >
VelocityCoupling2D<T,DESCRIPTOR1,DESCRIPTOR2>* VelocityCoupling2D<T,DESCRIPTOR1,DESCRIPTOR2>::clone() const
{
    return new VelocityCoupling2D<T,DESCRIPTOR1,DESCRIPTOR2>(*this);
}


template<typename T,
         template<typename U1> class DESCRIPTOR1,
         template<typename U2> class DESCRIPTOR2 >
void VelocityCoupling2D<T,DESCRIPTOR1,DESCRIPTOR2>::getTypeOfModification(std::vector<modif::ModifT>& modified) const
{
    modified[0] = modif::nothing;
    modified[1] = modif::staticVariables;
}

int main(int argc, char* argv[])
{
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");

    // Physical parameters (chosen by the user).

    T lx = 0.05;
    T ly = 0.05;
    T uPhys = 0.02;
    T nuPhys = 0.00001;

    // Numerical parameters (need to be filled by the participants).

    uLB = 0.01;
    plint N = 256;

    dx = lx / ((T) N - 1);
    nx = util::roundToInt(lx / dx) + 1;
    ny = util::roundToInt(ly / dx) + 1;

    dt = dx * uLB / uPhys;

    T nuLB = nuPhys * dt / (dx * dx);

    T tau = NS_DESCRIPTOR<T>::invCs2 * nuLB + 0.5;
    T omega = 1.0 / tau;

    // Iteration intervals for output and end of the simulation

    plint logIter   = 50;
    plint imIter    = 100;
    plint vtkIter   = 500;
    plint maxIter   = 100000;

    // Print key parameters.

    pcout << "dx = " << dx << std::endl;
    pcout << "nx = " << nx << std::endl;
    pcout << "ny = " << ny << std::endl;
    pcout << "dt = " << dt << std::endl;
    pcout << "nuPhys = " << nuPhys << std::endl;
    pcout << "nuLB = " << nuLB << std::endl;
    pcout << "tau = " << tau << std::endl;
    pcout << "omega = " << omega << std::endl;
    pcout << "Re = " << uPhys * lx / nuPhys << std::endl;

    double omega_ad = 1 / 0.6;

    MultiBlockLattice2D<T, NS_DESCRIPTOR> ns_lattice (
              nx, ny,
              new BGKdynamics<T,NS_DESCRIPTOR>(omega) );

    MultiBlockLattice2D<T, AD_DESCRIPTOR> ad_lattice (
              nx, ny,
              new AdvectionDiffusionBGKdynamics<T,AD_DESCRIPTOR>(omega_ad) );
    ad_lattice.periodicity().toggleAll(true);


    plint processorLevel = 1;
    integrateProcessingFunctional (
            new VelocityCoupling2D<T, NS_DESCRIPTOR, AD_DESCRIPTOR>(), 
            ns_lattice.getBoundingBox(), ns_lattice, ad_lattice, processorLevel );

    Box2D leftDomain(ad_lattice.getBoundingBox());
    leftDomain.x1 = leftDomain.x1 / 4;
    initializeAtEquilibrium(ad_lattice, leftDomain, 2.0, Array<T,2>(0.,0.));
    ad_lattice.initialize();

    OnLatticeBoundaryCondition2D<T,NS_DESCRIPTOR>*
        boundaryCondition = createLocalBoundaryCondition2D<T,NS_DESCRIPTOR>();

    cavitySetup(ns_lattice, *boundaryCondition);

    T previousIterationTime = T();

    // Main loop over time iterations.
    for (plint iT=0; iT<maxIter; ++iT) {
        global::timer("mainLoop").restart();

        if (iT%imIter==0) {
            pcout << "Saving PPM image ..." << std::endl;
            writePpm(ns_lattice, ad_lattice, iT);
            pcout << std::endl;
        }

        if (iT%vtkIter==0) {
            pcout << "Saving VTK file ..." << std::endl;
            writeVTK(ns_lattice, iT);
        }

        if (iT%logIter==0) {
            pcout << "step " << iT
                  << "; t=" << iT*dt;
        }

        // Lattice Boltzmann iteration step.
        ad_lattice.collideAndStream();
        ns_lattice.collideAndStream();

        if (iT%logIter==0) {
            pcout << "; av energy="
                  << std::setprecision(10) << getStoredAverageEnergy(ns_lattice)
                  << "; av rho="
                  << getStoredAverageDensity(ns_lattice) << std::endl;
            pcout << "Time spent during previous iteration: "
                  << previousIterationTime << std::endl;
        }

        previousIterationTime = global::timer("mainLoop").stop();
    }

    delete boundaryCondition;

    return 0;
}
