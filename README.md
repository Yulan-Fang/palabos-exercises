# Exercises for the Palabos class

## Cloning the repo

This repository contains the [palabos](https://gitlab.com/unigespc/palabos) as
a submodule to clone it:

```git
git clone --recurse-submodules git@gitlab.com:unigespc/palabos-exercises.git
```

## Pulling the latest version

If you already cloned the repo and want to pull the latest version:

```git
git pull --recurse-submodules 
```

For more informations see [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
