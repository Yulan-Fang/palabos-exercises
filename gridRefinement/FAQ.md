# Different possible errors

## offLatticeExternalFlow

* The error message:
    ```
    Usage: ./offLatticeExternalFlow_exercise xml-input-file-name [restart]
    ```

    Means they forgot the config file:
    ```
    ./offLatticeExternalFlow_exercise config.xml
    ```
* The error message:
    ```
    Caught exception in process 0. Palabos IO exception: gridDensityFunction.dat: Cannot open file for reading; No such file or directory
    ```

    Means they forgot (or misnamed) the `gridDensityFunction.dat`. They can either change the name of the file or change the `config.xml` file.


