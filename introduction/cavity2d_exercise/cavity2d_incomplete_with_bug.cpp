/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2017 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
  * Flow in a 2D cavity.
  **/

#include "palabos2D.h"
#include "palabos2D.hh"   // include full template code

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace plb;
using namespace plb::descriptors;

typedef double T;
#define DESCRIPTOR D2Q9Descriptor

// Global variables for conveniently passing data in functions.

static plint nx, ny;
static T dx, dt;
static T uLB;

void cavitySetup(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
        OnLatticeBoundaryCondition2D<T,DESCRIPTOR>& boundaryCondition)
{
    Box2D top(0, nx-1, ny-1, ny-1);
    Box2D bottom(0, nx-1, 0,    0);
    Box2D left(0, 0, 1, ny-2);
    Box2D right(nx-1, nx-1, 1, ny-2);

    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, top);
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, bottom);
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, left);
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice, right);

    T u = uLB;
    setBoundaryVelocity(lattice, top, Array<T,2>((T)0.,u) );
    setBoundaryVelocity(lattice, bottom, Array<T,2>((T)0.,(T)0.) );
    setBoundaryVelocity(lattice, left, Array<T,2>((T)0.,(T)0.) );
    setBoundaryVelocity(lattice, right, Array<T,2>((T)0.,(T)0.) );

    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );
    initializeAtEquilibrium(lattice, top, (T)1., Array<T,2>((T)0.,u) );

    lattice.initialize();
}

template<class BlockLatticeT>
void writePpm(BlockLatticeT& lattice, plint iter)
{
    static plint iFile = 0;

    // iFile = iter;
    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledPpm(createFileName("uNorm", iFile, 6), *computeVelocityNorm(lattice));
    imageWriter.writeScaledPpm(createFileName("logUnorm", iFile, 6),
            *computeLog(*add((T)1.e-8,*computeVelocityNorm(lattice))));

    iFile++;
}

template<class BlockLatticeT>
void writeVTK(BlockLatticeT& lattice, plint iter)
{
    static plint iFile = 0;

    // iFile = iter;
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iFile, 6), dx);
    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", dx/dt);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", dx/dt);

    iFile++;
}

int main(int argc, char* argv[])
{
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");

    // Physical parameters (chosen by the user).

    T lx = 0.05;
    T ly = 0.05;
    T uPhys = 0.02;
    T nuPhys = 0.00001;

    // Numerical parameters (need to be filled by the participants).

    uLB = 0.01;
    plint N = 256;

    dx = ;
    nx = ;
    ny = ;

    dt = ;

    T nuLB = ;

    T tau = ;
    T omega = ;

    // Iteration intervals for output and end of the simulation

    plint logIter   = 50;
    plint imIter    = 100;
    plint vtkIter   = 500;
    plint maxIter   = 100000;

    // Print key parameters.

    pcout << "dx = " << dx << std::endl;
    pcout << "nx = " << nx << std::endl;
    pcout << "ny = " << ny << std::endl;
    pcout << "dt = " << dt << std::endl;
    pcout << "nuPhys = " << nuPhys << std::endl;
    pcout << "nuLB = " << nuLB << std::endl;
    pcout << "tau = " << tau << std::endl;
    pcout << "omega = " << omega << std::endl;
    pcout << "Re = " << uPhys * lx / nuPhys << std::endl;

    //T cSmago = 0.11;
    MultiBlockLattice2D<T, DESCRIPTOR> lattice (
              nx, ny,
              new BGKdynamics<T,DESCRIPTOR>(omega) );
              //new SmagorinskyBGKdynamics<T,DESCRIPTOR>(omega, cSmago) );

    OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
        boundaryCondition = createLocalBoundaryCondition2D<T,DESCRIPTOR>();

    cavitySetup(lattice, *boundaryCondition);

    T previousIterationTime = T();

    // Main loop over time iterations.
    for (plint iT=0; iT<maxIter; ++iT) {
        global::timer("mainLoop").restart();

        if (iT%imIter==0) {
            pcout << "Saving PPM image ..." << std::endl;
            writePpm(lattice, iT);
            pcout << std::endl;
        }

        if (iT%vtkIter==0) {
            pcout << "Saving VTK file ..." << std::endl;
            writeVTK(lattice, iT);
        }

        if (iT%logIter==0) {
            pcout << "step " << iT
                  << "; t=" << iT*dt;
        }

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();

        if (iT%logIter==0) {
            pcout << "; av energy="
                  << std::setprecision(10) << getStoredAverageEnergy(lattice)
                  << "; av rho="
                  << getStoredAverageDensity(lattice) << std::endl;
            pcout << "Time spent during previous iteration: "
                  << previousIterationTime << std::endl;
        }

        previousIterationTime = global::timer("mainLoop").stop();
    }

    delete boundaryCondition;

    return 0;
}
